package org.inguelberth.test;

import org.inguelberth.db.Conexion;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Prueba{
	public static void main(String args[]){
		Conexion cnx = new Conexion();
		cnx.ejecutarSentencia("INSERT INTO Usuario(nombre, pass, idRol) VALUES ('Paniagua','222',1)");
		ResultSet resultado = cnx.ejecutarConsulta("SELECT Usuario.idUsuario,Usuario.nombre, Usuario.pass,Rol.nombre AS Rol FROM Usuario INNER JOIN Rol ON Usuario.idRol=Rol.idRol");
		try{
			if(resultado != null){
				while(resultado.next()){
					System.out.println("-----------------------");
					System.out.println("id: "+resultado.getInt("idUsuario"));
					System.out.println("nombre: "+resultado.getString("nombre"));
					System.out.println("pass: "+resultado.getString("pass"));
					System.out.println("rol: "+resultado.getString("Rol"));
				}
			}
		}catch(SQLException sql){
			sql.printStackTrace();
		}
	}
}
